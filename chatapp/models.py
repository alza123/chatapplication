from django.db import models


class Message(models.Model):
    room = models.TextField(max_length=200)
    username = models.TextField(max_length=200)
    text = models.TextField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)
