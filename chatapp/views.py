from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, UserRegistrationForm
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from random import randint

from .models import Message


@login_required
def index(request):
    messages = Message.objects.all()

    return render(request, 'chatapp/index.html', {
        "random": str(randint(1, 99999999999999999)),
        "messages": messages
    })


@login_required
def room(request, room_name):
    messages = Message.objects.all().filter(room=room_name)

    return render(request, 'chatapp/room.html', {
        "random": str(randint(1, 99999999999999999)),
        "room_name": room_name,
        "messages": messages
    })


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request, username=cd['username'], password=cd['password'])

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('index')
            else:
                return redirect('login')
    else:
        form = LoginForm()
    return render(request, 'chatapp/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return redirect('login')


def register(request):
    if request.method == "POST":
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return render(request, 'chatapp/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'chatapp/register.html', {'user_form': user_form})
